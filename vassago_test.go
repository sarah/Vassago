package vassago

import (
	"testing"
)

func TestVassago(t *testing.T) {

	const NumberOfServerBuckets = 50
	const NumberOfRecords = 2048*100

	client := NewClient(NumberOfServerBuckets, 255)
	server := NewServer(NumberOfServerBuckets, NumberOfRecords, 1)

	for i:=0;i< NumberOfServerBuckets*NumberOfRecords; i++ {
		server.AddItem([]byte{byte(i)})
	}

	request := client.GenerateRequest(3)
	response := server.PrivateLookup(request)
	result, err := client.DecryptResponse(response)

	if err == nil {
		t.Logf("Returned: (%d) %d", len(result), result)
	} else {
		t.Errorf("Did not get expected result: %x %v", result, err)
	}
}
