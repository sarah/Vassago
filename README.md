# Vassago: Private Information Retrieval


Vassago is a client/server implementation of PIRMAP[1], an efficient single-server PIR focused on
retreiving large information in a metadata-resisant way.

Vassago buckets records by recency e.g. a request for bucket 0 will return the bucket with the most recently added items.

This library is a work in progress, unvetted, unaudited. Please do not use this unless you understand the risks of doing so.

* [1] Mayberry, Travis, Erik-Oliver Blass, and Agnes Hui Chan. "PIRMAP: Efficient private information retrieval for MapReduce." International Conference on Financial Cryptography and Data Security. Springer, Berlin, Heidelberg, 2013.
